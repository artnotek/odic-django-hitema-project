from django.contrib import admin

# Register your models here.
from django.contrib import admin
from django.utils.text import Truncator

from .models import User, Party, Drawings, PartyPlayers


class UserAdmin(admin.ModelAdmin):
    list_display = ('pseudo', 'password')
    # list_filter = ('pseudo')
    # date_hierarchy = ('date')
    # ordering = ('pseudo')
    # search_fields = ('pseudo')

    # def apercu_contenu(self, article):
    #     """
    #     Retourne les 40 premiers caractères du contenu de l'article,
    #     suivi de points de suspension si le texte est plus long.
    #
    #     On pourrait le coder nous même, mais Django fournit déjà la
    #     fonction qui le fait pour nous !
    #     """
    #     return Truncator(article.contenu).chars(30, truncate='...')
    #
    # # En-tête de notre colonne
    #
    # apercu_contenu.short_description = 'Aperçu du contenu'


admin.site.register(User, UserAdmin)
admin.site.register(Party)
admin.site.register(Drawings)
admin.site.register(PartyPlayers)

