# Generated by Django 2.1.5 on 2020-06-09 22:11

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('drawing', '0002_auto_20200601_2245'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='user',
            name='userRole',
        ),
    ]
