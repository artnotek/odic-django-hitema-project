from django.urls import path, include
from . import views
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    path('', views.home, name="accueil"),
    path('findgames/', views.findgame, name="liste_parties"),
    path('detailsgame/<id_partie>', views.detailsgame, name="details_partie"),
    path('creategame/', views.creategame, name="creer_partie"),
    path('myaccount/', views.myaccount, name="mon_compte"),
    path('login/', views.login, name="login"),
    path('register/', views.register, name="register"),
    path('drawing/<int:id_partie>', views.drawing, name="drawing"),
    path('logout/', views.logout, name="deconnexion"),
    path('joingame/<int:id_partie>', views.joingame, name="rejoindre_partie"),
    path('endgame/<int:id_partie>', views.endgame, name="fin_de_partie")
]