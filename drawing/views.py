from datetime import datetime

from django.shortcuts import render, redirect
from django.http import HttpResponse, response

# Create your views here.
from drawing.forms import PartyForm, RegisterForm, LoginForm, UserAccount
from drawing.models import Party, User, PartyPlayers, Drawings
from django.core import serializers
from django.http import HttpResponseRedirect


def home(request):
    # la page d'accueil propose également un formulaire de connexion
    form = LoginForm(request.POST or None)
    return render(request, 'base.html', locals())


def findgame(request):
    # service pour l'affichage de toutes les sessions de dessins actuellement ouvertes
    gameslists = Party.objects.all()
    return render(request, 'drawing/findgames.html', locals())


def detailsgame(request, id_partie):
    gamedetails = Party.objects.filter(id=id_partie)
    usersinparty = PartyPlayers.objects.filter(partyId=id_partie)
    usernames = []
    drawingsinparty = Drawings.objects.filter(partyId=id_partie)
    drawinglink = []
    userlinkbelonging = []
    creatorid = Party.objects.get(id=id_partie)
    creatorid = creatorid.creatorName
    sessionid = User.objects.get(id=request.session['id'])
    sessionid = sessionid.pseudo
    theme = Party.objects.get(id=id_partie)
    theme = theme.theme
    for e in usersinparty:
        user = User.objects.filter(id=e.userId)
        for f in user:
            user = f.pseudo
        usernames.append(user)
    for i in drawingsinparty:
        drawind = User.objects.filter(id=id_partie)
        userdrawind = User.objects.filter(id=e.userId)
        for j in drawingsinparty:
            drawind = j.link
            userdrawind = j.userId
        drawinglink.append(drawind)
        userlinkbelonging.append(userdrawind)
    drawinglink = list(reversed(drawinglink))
    userlinkbelonging = list(reversed(userlinkbelonging))
    usernames = list(reversed(usernames))
    return render(request, 'drawing/detailsgame.html', locals())


def creategame(request):
    form = PartyForm(request.POST or None)
    if request.POST:
        # print('test', request.POST)
        for info in User.objects.filter(id=request.session['id']):
            pseudo = info.pseudo
            userid = info.id
            print(pseudo, userid)
        donnees = {
            'theme': form.data['theme'],
            'creatorId': userid,
            'creatorName': pseudo,
            'isActive': True
        }
        Party(theme=donnees['theme'],
              creatorId=donnees['creatorId'],
              creatorName=donnees['creatorName'],
              isActive=True).save()
        return redirect('liste_parties')
    return render(request, 'drawing/creategame.html', locals())


def endgame(request, id_partie):
    # id de la personne qui a cliqué sur le bouton
    userid = request.session['id']
    # id de la partie sélectionnée
    partyid = id_partie
    # id du propriétaire de la partie
    creatorid = Party.objects.get(id=id_partie)
    creatorid = creatorid.creatorId
    # print(userid, partyid, creatorid)
    if userid == creatorid:
        PartyPlayers.objects.filter(partyId=partyid).delete()
        Drawings.objects.filter(partyId=partyid).delete()
        Party.objects.filter(id=partyid).delete()
    # return render(redirect("accueil"))
    return render(request, 'drawing/findgames.html')


def joingame(request, id_partie):
    # id de la personne qui a cliqué sur le bouton
    userid = request.session['id']
    # id de la partie sélectionnée
    partyid = id_partie
    usertoadd = PartyPlayers(partyId=id_partie, userId=userid).save()
    return render(request, 'drawing/detailsgame.html', locals())


def myaccount(request):
    form = UserAccount(request.POST or None)
    if form.is_valid():
        edit = True
        username = User.objects.get(id=request.session['id'])
        username.pseudo = form.cleaned_data['pseudo']
        username.password = form.cleaned_data['password']
        username.save()
    return render(request, 'drawing/myaccount.html', locals())


def login(request):
    form = LoginForm(request.POST or None)

    if form.is_valid():
        nom = form.cleaned_data['pseudo']
        mdp = form.cleaned_data['password']
        for loginInfo in User.objects.filter(pseudo="{0}".format(nom)):
            if loginInfo.pseudo != nom:
                raise form.ValidationError("pseudo non reconnu !")
            else:
                if loginInfo.password != mdp:
                    raise form.ValidationError("mot de passe invalide !")
                else:
                    loginreussi = True
                    envoi = True
                    request.session['connected'] = 'ok'
                    request.session['id'] = loginInfo.id
                    print(nom, mdp, request.session['connected'], request.session['id'])
                    return redirect("accueil")
    return render(request, 'drawing/login.html', locals())


def register(request):
    form = RegisterForm(request.POST or None)

    if form.is_valid():
        pseudo = form.cleaned_data['pseudo']
        password = form.cleaned_data['password']
        form.save()
        return redirect("accueil")

    return render(request, 'drawing/register.html', locals())


def drawing(request, id_partie):
    userinfo = User.objects.get(id=request.session['id'])
    gamedetails = Party.objects.get(id=id_partie)
    name = userinfo.pseudo
    theme = gamedetails.theme
    if request.POST:
        # print(request.POST)
        print(request.POST['visuel'])
        pseudo = request.POST['pseudo']
        titre = request.POST['titre']
        image = request.POST['visuel']
        dessin = Drawings(link=image, userId=request.session['id'], partyId=id_partie).save()
        # retour = redirect('liste_parties', id_partie)
        # return retour
    return render(request, 'drawing/drawing.html', locals())


def logout(request):
    del request.session['connected']
    del request.session['id']
    return redirect("accueil")